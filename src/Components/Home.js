import React from "react";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div style={styles}>
      <div
        style={{ textAlign: "center", margin: 20, padding: 30 }}
        className="container"
      >
        <h1>Welcome!</h1>
        <div className="links">
          <Link to="/Login">LogIn</Link>
          <br />
          <br />
          <Link to="/SignUp">Sign UP</Link>
        </div>
      </div>
    </div>
  );
}

const styles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

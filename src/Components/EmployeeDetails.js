import React from "react";
import { useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import Error from "./Error";

export default function EmployeeDetails(props) {
  const id = props.match.params.id;
  const listOfEmployees = useSelector((state) => state.EmployReducer.employees);
  let emp = listOfEmployees.find((user) => user.id === id);
  let token = useSelector((state) => state.LoginReducer.id);

  const history = useHistory();
  if (emp) {
    // empName=emp.data.name;
    // department=emp.data.department;
    // project=emp.data.project;
    // date=emp.data.proletject
    localStorage.setItem("name", emp.data.name);
    localStorage.setItem("department", emp.data.department);
    localStorage.setItem("project", emp.data.project);
    localStorage.setItem("date", emp.data.date);
  }
  if (typeof localStorage !== "undefined") {
    token = localStorage.getItem("token");
    let empName = localStorage.getItem("name");
    let department = localStorage.getItem("department");
    let project = localStorage.getItem("project");
    let date = localStorage.getItem("date");
    emp = {
      data: {
        name: empName,
        department: department,
        project: project,
        date: date,
      },
    };
  }

  const clearToken = () => {
    if (typeof localStorage !== "undefined") {
      localStorage.removeItem("token");
    }
    history.push("/login");
  };
  if (token) {
    return (
      <div className="showDetail">
        <h3 style={{ textAlign: "center", margin: 30 }}>Employee details</h3>
        <div className="cart">
          <div className="employeedetails">
            Name : <h5 style={{ display: "inline" }}>{emp.data.name}</h5> <br />
            Joining date : <p style={{ display: "inline" }}>
              {emp.data.date}
            </p>{" "}
            <br />
            Department :{" "}
            <p style={{ display: "inline" }}>{emp.data.department}</p> <br />
            Project : <p style={{ display: "inline" }}>
              {emp.data.project}
            </p>{" "}
            <br />
          </div>
        </div>
        <div>
          <Link style={{ marginLeft: 480, marginTop: 100 }} to="/emplist">
            Back
          </Link>
        </div>
        <div>
          <Link
            onClick={clearToken}
            style={{ marginLeft: 480, marginTop: 100 }}
            to="/login"
          >
            LogOut
          </Link>
        </div>
      </div>
    );
  } else {
    return <Error />;
  }
}

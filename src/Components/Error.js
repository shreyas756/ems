import React from "react";
import { Link } from "react-router-dom";

export default function Error() {
  return (
    <div className="error">
      <h1 style={{ textAlign: "center" }}>Something Went Wrong</h1>
      <div className="default">
        <div className="message">
          <br />
          <br />
          <h2 style={{ textAlign: "center" }}>
            Please{" "}
            <Link style={{ textDecoration: "none" }} to="/SignUp">
              SignUp
            </Link>{" "}
            or{" "}
            <Link style={{ marginTop: 20, textDecoration: "none" }} to="/Login">
              Login
            </Link>{" "}
            again
          </h2>
          <br />
        </div>
      </div>
    </div>
  );
}

import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addUserSignUp } from "../app/index";
import { useHistory } from "react-router";
import "./Login.css";

export default function SignUp() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [department, setDepartment] = useState("");
  const [role, setRole] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();

  const changeInput = (event) => {
    event.target.type === "password"
      ? setPassword(event.target.value)
      : setName(event.target.value);
  };

  const handleEmail = (event) => {
    setEmail(event.target.value);
  };
  const handleDepartment = (event) => {
    setDepartment(event.target.value);
  };
  const handleRole = (event) => {
    setRole(event.target.value);
  };
  const formSubmit = (event) => {
    event.preventDefault();
    dispatch(
      addUserSignUp({ name, password, email, department, project: role })
    );
    //Here we need to store that username and password
    setName("");
    setDepartment("");
    setPassword("");
    setRole("");
    setEmail("");
    history.push("/login");
  };
  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Create Account</h1>
      <div className="SignUp">
        <form className="SignUp__form">
          <input
            onChange={changeInput}
            value={name}
            className="s-input"
            type="text"
            placeholder="Enter name"
          />{" "}
          <br />
          <input
            onChange={changeInput}
            value={password}
            className="s-input"
            type="password"
            placeholder="Enter password"
          />{" "}
          <br />
          <input
            onChange={handleEmail}
            value={email}
            className="s-input"
            type="text"
            placeholder="Enter email"
          />{" "}
          <br />
          <input
            onChange={handleDepartment}
            value={department}
            className="s-input"
            type="text"
            placeholder="Department name"
          />{" "}
          <br />
          <input
            onChange={handleRole}
            value={role}
            className="s-input"
            type="text"
            placeholder="Role"
          />{" "}
          <br />
          <button type="submit" className="submit__btn" onClick={formSubmit}>
            Submit
          </button>
          <Link className="s-input" to="/">
            Home
          </Link>
          <Link className="s-input" to="/login">
            LogIn
          </Link>
        </form>
      </div>
    </div>
  );
}

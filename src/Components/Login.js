import React, { useState } from "react";
import "./Login.css";
import { useHistory } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { addToken } from "../app/index";
import { Link } from "react-router-dom";

const Login = () => {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  let users = useSelector((state) => state.userReducer.users);
  const dispatch = useDispatch();
  const history = useHistory();
  const handleSubmit = (e) => {
    e.preventDefault();
    let logedInUser = users.find(
      (user) => user.user.name === name && user.user.password === password
    );
    if (!logedInUser) {
      history.push("/error");
    } else {
      console.log("loggedin");
      if (typeof localStorage !== "undefined") {
        localStorage.setItem("token", logedInUser.id);
      }
      dispatch(addToken(logedInUser.id));
      setName("");
      setPassword("");
      history.push("/emplist");
    }
  };

  return (
    <div className="login">
      <form className="login__form" onSubmit={(e) => handleSubmit(e)}>
        <h1>Login here 🚪</h1>
        <input
          type="name"
          placeholder="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <input
          type="password"
          value={password}
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <button type="submit" className="submit__btn">
          Submit
        </button>
        <br />
        <Link to="/SignUp">SignUp</Link>
      </form>
    </div>
  );
};

export default Login;

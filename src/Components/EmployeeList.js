import React, { useState } from "react";
import {
  EditOutlined,
  DeleteOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import { deleteEmployee, addEmployee } from "../app/index";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import "./Login.css";
import Error from "./Error";

export default function EmployeeList() {
  const [name, setName] = useState("");
  const [department, setDepartment] = useState("");
  const [project, setProject] = useState("");
  const [date, setDate] = useState("");
  const dispatch = useDispatch();

  const listOfEmployees = useSelector((state) => state.EmployReducer.employees);
  let token = useSelector((state) => state.LoginReducer.id);
  if (typeof localStorage !== "undefined") {
    token = localStorage.getItem("token");
    // listOfEmployees = localStorage.getItem("listOfEmployee");
  }
  const editEmployee = (eId) => {
    const needToEditEmp = listOfEmployees.filter((emp) => emp.id === eId)[0];
    dispatch(deleteEmployee(eId));
    setName(needToEditEmp.data.name);
    setDepartment(needToEditEmp.data.department);
    setProject(needToEditEmp.data.project);
    setDate(needToEditEmp.data.date);
  };

  const changeDate = (event) => {
    let pickDate = event.target.value.toString();
    setDate(pickDate);
  };
  const changeName = (event) => {
    setName(event.target.value);
  };
  const changeDepartment = (event) => {
    setDepartment(event.target.value);
  };
  const changeProject = (event) => {
    setProject(event.target.value);
  };

  const addEmp = (event) => {
    event.preventDefault();
    if (name.length <= 0) {
      alert("Please Enter valid employee name");
    } else if (department.length <= 0) {
      alert("Please Enter assigned department");
    } else if (project.length <= 0) {
      alert("Please Enter project name");
    } else if (date.length <= 0) {
      alert("Please Enter a joining date");
    } else {
      // let data = {
      //   name: name,
      //   department: department,
      //   project: project,
      //   date: date,
      // };
      // if (typeof localStorage !== "undefined") {
      //   localStorage.setItem("name", name);
      // }
      dispatch(addEmployee({ name, department, date, project }));
      setName("");
      setDate("");
      setDepartment("");
      setProject("");
    }
  };

  if (!token) {
    return <Error />;
  } else {
    return (
      <>
        <div className="employeeList">
          <h2 style={{ textAlign: "center", margin: 40, padding: 40 }}>
            List of Employees
          </h2>
          <div className="createEmployeeData">
            <input
              onChange={changeName}
              type="text"
              placeholder="Name"
              value={name}
            />
            <br />
            <input
              onChange={changeDepartment}
              type="text"
              placeholder="Department"
              value={department}
            />
            <br />
            <input
              onChange={changeProject}
              type="text"
              placeholder="Project"
              value={project}
            />
            <br />
            <input type="date" value={date} onChange={changeDate} />
            <div className="upper">
              <UserAddOutlined
                onClick={addEmp}
                style={{ marginTop: 10, color: "blue" }}
              />
            </div>
          </div>
          {listOfEmployees.map((emp) => (
            <div key={emp.id}>
              <div className="listEmployee">
                <div className="employeeList">
                  <h5>{emp.data.name}</h5>
                  <div>
                    <Link className="showDetails" to={`/empDetails/${emp.id}`}>
                      Show Details
                    </Link>
                    <br />
                    <DeleteOutlined
                      style={{ color: "red", marginLeft: 20 }}
                      onClick={() => dispatch(deleteEmployee(emp.id))}
                    />
                    <EditOutlined
                      style={{ color: "blue", marginLeft: 20 }}
                      onClick={() => editEmployee(emp.id)}
                    />
                  </div>
                </div>
              </div>
            </div>
          ))}
          <div>
            <Link className="Logout" to={"/login"}>
              LogOut
            </Link>
          </div>
        </div>
      </>
    );
  }
}

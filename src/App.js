import Home from "./Components/Home";
import Login from "./Components/Login";
import Error from "./Components/Error";
import Navbar from "./Components/Navbar";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import SignUp from "./Components/SignUp";
import EmployeeList from "./Components/EmployeeList";
import EmployeeDetails from "./Components/EmployeeDetails";

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <div className="content">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route exact path="/signUp">
              <SignUp />
            </Route>
            <Route exact path="/emplist">
              <EmployeeList />
            </Route>
            <Route
              exact
              path="/empDetails/:id"
              component={EmployeeDetails}
              render={(props) => {
                <EmployeeDetails {...props} />;
              }}
            />
            <Route exact path="/error">
              <Error />
            </Route>
            <Route path="*">
              <Error />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
